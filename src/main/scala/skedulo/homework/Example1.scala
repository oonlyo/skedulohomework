package skedulo.homework

/* Write a function that prints every number from 1 to 100 
except­  
For every number divisible by 4, print “Hello” instead 
For every number divisible by 5, print “World” instead 
If a number is divisible by both, print “HelloWorld” instead */
object Example1 {
  def HelloWorld(): Unit = {
    val numbers = 1 to 100
    numbers.map(n => {
      print(s"$n ")
      if (n % 4 == 0 && n % 5 == 0) {
        println("HelloWorld")
      }
      else if (n % 4 == 0) {
        println("Hello")
      }
      else if (n % 5 == 0) {
        println("World")
      }
      else {
        println()
      }
    })
  }
}
