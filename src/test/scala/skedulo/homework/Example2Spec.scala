package skedulo.homework

import org.scalatest.FlatSpec

class Example2Spec extends FlatSpec {
  "tables" should "be joined" in {
    val exam2 = new Example2(Map(
      (1, new Product(1,"a", 1.0f)),
      (2, new Product(2,"b", 2.0f)),
      (3, new Product(3,"c", 3.0f))
    ))

    val results = exam2.GetOrderProductsDetails(Array[OrderProduct](
      new OrderProduct(11,1),
      new OrderProduct(111,1),
      new OrderProduct(22,2))
    )

    assert(results.size == 3)
    assert(results(11).Name=="a")
    assert(results(111).Name=="a")
    assert(results(22).Name=="b")
  }
}