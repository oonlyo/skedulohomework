package skedulo.homework
/*
Write a function that takes an Array of “OrderProducts” and populates the “Name”
 and “Price” fields on each OrderProduct with the relevant Product “Name” and “Price”
 field from the Product table.
 Your function must be able to handle up to 100 records at a time.
 The database server is connected via a high latency connection, so to optimize
 performance you should minimize the number of queries you perform
 */
// Assume Products are data in a relational database, use a Map[Int, Products] to simulate index -> others
class Example2 (val Products: Map[Int, Product]) {

  //the output is a Map[Int, Product] to represent orderProductId -> Product
  def GetOrderProductsDetails(orderProducts: Array[OrderProduct]): Map[Int, Product] = {
    val products = GetProducts(orderProducts.map( op => op.ProductId))
    orderProducts.map(op => (op.OrderId, products(op.ProductId))).toMap
  }

  def GetProducts(productIds: Array[Int]): Map[Int, Product] = {
    productIds.map(id => (id, Products(id))).toMap
  }
}

class OrderProduct(val OrderId: Int, val ProductId: Int)

class Product(val ProductId: Int, val Name: String, val Price: Float)
