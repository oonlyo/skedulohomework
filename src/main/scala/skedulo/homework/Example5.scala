package skedulo.homework

/* Given an array of records, calculate the optimal schedule.*/
object Example5 {
  //the output is a Map[Int, (String, Int)] to represent on which minute -> to see which performance
  def CalcOptimalSchedule(performances: Array[Performance]): Map[Int,Performance] = {
    performances.map(p => {
      val minutes = p.Start to p.Finish
      minutes.map(m =>
        (m,p)
      ).toMap
    }).reduce((performance1,performance2) =>
      (performance1.keySet++performance2.keySet).map(minute => {
        if(performance1.get(minute).isEmpty && performance2.get(minute).isEmpty)
          throw new Exception("two items in reduce stage are both invalid!")
        else if(performance1.get(minute).isEmpty)
          (minute, performance2(minute))
        else if(performance2.get(minute).isEmpty)
          (minute, performance1(minute))
        else if(performance1(minute).Priority < performance2(minute).Priority)
          (minute, performance2(minute))
        else
          (minute, performance1(minute))
      }).toMap
    )
  }
}

//for the sake of simplicity, use Int to represent datetime by minute basis
class Performance(val Band: String, val Start: Int, val Finish: Int, val Priority: Int)