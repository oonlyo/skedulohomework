package skedulo.homework

import org.scalatest.FlatSpec
import skedulo.homework.Example5.CalcOptimalSchedule

class Example5Spec extends FlatSpec {
  "Optimal schedule" should "be calculated" in {
    val performances = Array(
      new Performance("a",1,5,1),
      new Performance("b",2,4,2),
      new Performance("c",2,3,3),
      new Performance("d",7,9,2)
    )

    val results = CalcOptimalSchedule(performances)
    assert(results.size==8)
    assert(results(1).Band=="a")
    assert(results(2).Band=="c")
    assert(results(3).Band=="c")
    assert(results(4).Band=="b")
    assert(results(5).Band=="a")
    assert(results.get(6).isEmpty)
    assert(results(7).Band=="d")
    assert(results(8).Band=="d")
    assert(results(9).Band=="d")
  }
}
